﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reflection_hw
{
    class Program
    {
        static void Main(string[] args)
        {
            Type type = typeof(Console);
            try
            {
                foreach(var element in type.GetMethods())
                {
                    Console.WriteLine(element);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadLine();
        }
    }
}
